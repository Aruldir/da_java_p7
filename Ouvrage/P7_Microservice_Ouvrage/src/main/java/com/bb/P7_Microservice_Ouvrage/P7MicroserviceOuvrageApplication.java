package com.bb.P7_Microservice_Ouvrage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class P7MicroserviceOuvrageApplication {

	public static void main(String[] args) {
		SpringApplication.run(P7MicroserviceOuvrageApplication.class, args);
	}

}
