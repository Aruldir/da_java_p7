package com.bb.P7_Microservice_Ouvrage.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OuvrageController {
	
	@GetMapping("/Ouvrages")
	public String listeOuvrages() {
		return "ListeOuvrages";
	}
}
